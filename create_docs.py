from datetime import date
import json
import os
import time
import re
import os.path
import shutil
from docxtpl import DocxTemplate
from includes.string_ends_to_remove import stringEndsToRemove
from includes.get_address import getAddress

today = date.today()

# TODO: Separate out shared code / functions?

# Create folder for documents
# (Deletes folder if it exists for current day 
# - assuming it's easier for development purposes)
def create_folder():
  folderName = f'cambridge-{today.strftime("%d%m%y")}'
  
  # If directory exists, delete it
  if os.path.isdir(f'docs/{folderName}'):
    print(f'[i] {folderName} exists...removing it')
    shutil.rmtree(f'docs/{folderName}')
  
  # Create it
  os.mkdir(os.getcwd() + f'/docs/{folderName}')
  print(f'[i] {folderName} Directory Created')
  
  # Return folder name for use later
  return folderName

# Iterate data (from file) and create 
# letter documents for each venue
def create_documents(data, folderName, template):
  print('[i] Creating documents')
  i=0
  for venue in data.values():
    i = i + 1
    vn = venue['name']
    print(f'[{i}] {vn} - Creating File')
    create_document(venue, folderName, template)
    print(f'[{i}] {vn} - Created')

# Ensure special characters are shown on labels
# Add extra chars here if and when necessary
def encodeSpecialChars(s):
  return s.replace("&", "&#38;")

# Remove substring from end of string
def endchop(s, suffix):
  if suffix and s.endswith(suffix):
    return s[:-len(suffix)]
  return s

# Iterate the array of end strings to check for and remove them
def cleanUpNameEnd(venueName):
  for end in stringEndsToRemove:
    venueName = endchop(venueName, end)
  return venueName

# Create a single Word document
def create_document(venue, folderName, template):
  if "/" in venue['name']:
    raise Exception("Is there a '/' in this name? - " + venue['name'] + " - If so, it needs to be removed.") from None

  venue['name'] = cleanUpNameEnd(venue['name'])

  # Non-Windowed
  if template == 'n':
    doc = DocxTemplate("templates/letter.docx")
    context = {
      'date': today.strftime("%B %d, %Y"),
      'venueName': encodeSpecialChars(venue['name'])
    }
  # Windowed
  else:
    doc = DocxTemplate("templates/letter-with-address.docx")
    address = getAddress(venue['addresses'][0])
    context = {
      'date': today.strftime("%B %d, %Y"),
      'venueName': encodeSpecialChars(venue['name']),
      'addr_line_1': encodeSpecialChars(address[0]),
      'addr_line_2': encodeSpecialChars(address[1]),
      'addr_line_3': encodeSpecialChars(address[2]),
      'addr_line_4': encodeSpecialChars(address[3]),
      'addr_line_5': encodeSpecialChars(address[4]),
      'addr_line_6': encodeSpecialChars(address[5])
    }     

  doc.render(context)

  filename = venue['name'].replace(" ", "-").lower()
  # If file with name exists, append seconds to ensure correct number are created  
  if os.path.isfile(f'docs/{folderName}/{filename}.docx'):
    filename = filename + f'-{time.time()}'

  doc.save(f'docs/{folderName}/{filename}.docx')

# Start
if __name__ == '__main__':
  # check template
  template = input('Which template should be used?  Windowed [w] or Non-Windowed [n]: ')
  while template is "":
    template = input('Which template should be used?  Windowed [w] or Non-Windowed [n]: ')

  # create docs
  with open('data.json') as potential_users:
    print('[i] File Read')
    data = json.load(potential_users)
    folderName = create_folder()
    create_documents(data, folderName, template)
    print('Completed.')
