certifi==2020.12.5
chardet==4.0.0
cycler==0.10.0
docxtpl==0.11.3
googlemaps==4.4.5
idna==2.10
Jinja2==2.11.3
kiwisolver==1.3.1
lxml==4.6.3
MarkupSafe==1.1.1
numpy==1.20.1
Pillow==8.1.2
pyparsing==2.4.7
python-dateutil==2.8.1
python-docx==0.8.10
requests==2.25.1
six==1.15.0
urllib3==1.26.4
