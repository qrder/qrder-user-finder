# qrder-user-finder

Simple Python script to find restaurants, cafes, pubs and bars in a nearby area based on the latitude and longitude given.

## Prerequisites

Make sure Python 3.x is installed (preferably 3.5+) (it should come with pip3 as well).

## Setup / Installation

### Initial Setup 

Navigate to the project directory and run:

`python3 -m venv .venv`

This will (or should) setup the virtual environment to run the script.

Install the packages:

`pip3 install -r requirements.txt`


### Every Time before Development

You will then need to run this everytime you start development for the day:

`source .venv/bin/activate`

You can check if this has worked if your terminal starts with `(.venv)`.


## Running the Scripts

Either:

1. Install the `Python` VSCode extension.  Open a .py file.  Press the green play button in the top right.
   
2. Navigate to root directory and type: `python3 find_users.py` or `python3 create_docs.py`.

The output from `find_users.py` is used as the input for `create_docs.py`.  They are not immediately run together to allow time to validate the find_users output. 


## Workflow / Process

1. Open the `find_users.py` script.  Edit the `coords` variable to match your current location / desired area.  Edit the `radius` variable until the number of potential users returned in what we want (the script prints totals at the end of a run).  **IMPORTANT**: The Places API will only return a max of 60 results (or 3 pages) for a given radius - no matter how big it is.  To get more different results (if that makes sense?), you need to change the coords slightly.  Increasing the radius is mainly for areas that have fewer potential venues / users (the countryside, etc).
   
2. Run the `find_users.py` script.  This will generate a `data.json` file containing information about venues within the given radius.  It will also show a total number of venues found.
   
3. Once you are happy with the number of venues, scroll down the data.json file and check for these things:
   
    1. Check for franchises or snack bars / burger vans that have been missed.  Add these to the placesToIgnore list.  
   
    2. Quickly check that each venue has an address and it looks legit.  
   
    3. Check that each address has a VALID postcode - NP18 2NT is valid.  NP18 is not and will cause errors in the `create_labels.py` script later on.  Manually check these addresses and fix them inside the data.json file.  These changes will be overridden if the `find_users.py` script is run again so it's best to do this as late as possible in the workflow (it can even be left until right before we run the `create_labels.py` script).

4. Edit the letter template if needed. 
   
5. Run the `create_docs.py` script to generate all of the letters.  They will be generated inside the `docs/` directory.  A folder will be created inside of this docs directory.  It will have a location name and todays date. 

6. Check a few of the letters to ensure they generated properly - the layout is correct and the venue names filled in properly.
   
7. Run the `create_labels.py` script to generate Word documents containing 10 labels each.  They will be generated inside the `labels/` directory.  A folder will be created inside of this labels directory.  It will have a location name and todays date. 
   
8. Check a few of these documents to ensure the labels have been generated correctly and the addresses look correct.  It might be a good idea to check all of these documents as there will probably not be too many.  Check specifically that all addresses have a postcode and some sort of valid address - some addresses will look legit but may simply say 'Unnamed Road'.  These should be removed automatically by the `find_users` script.

9. Print the letters onto standard paper and the labels onto sticky printable labels (make sure the labels are face-down).  Make sure the printable labels are as far to the right as they can be in the printer (to make sure the margins are accurate for printing).

10. Once the letters and labels have been printed, we can run the `add_to_sent_list` script to make sure all of these places are not included in future searches.


## Templates

There are two templates that are currently used:

1. `letter.docx` contains the letter to send to customers.  This template should be edited directly if and when the letter needs to be updated.

2. `ten_labels.docx` contains a template for printing 10 sticky labels.  This is assuming the A4 sheet of sticky labels we have has 10 sticky labels on it.  If it does not, a new template will need to be created.  This will not be easy as the label template uses a specific code in the `Mailings --> Labels --> Options` settings.  For example, the ten_labels template uses one of these Avery label codes: `J8173` or `L7173`. 


## Generating requirements.txt

Enter the virtual environment using:

`source .venv/bin/activate`

Then type:

`pip3 freeze > requirements.txt`


## Packages

`find_users`:

The main package used is an API package to use the Google Maps API's easier:

GitHub Repo:

https://github.com/googlemaps/google-maps-services-python

GitHub Docs:

https://googlemaps.github.io/google-maps-services-python/docs/index.html#module-googlemaps


`create_docs`:

The main package used is called `python-docx`:

https://python-docx.readthedocs.io/en/latest/index.html