from docxtpl import DocxTemplate
import json
import os
import shutil
from datetime import date
from itertools import zip_longest
from includes.label_context import createContextObject 
from includes.get_address import getAddress

today = date.today()

# Group given array into n chunks 
def grouper(iterable, n, fillvalue=None):
  args = [iter(iterable)] * n
  return zip_longest(*args, fillvalue=fillvalue)

# Create folder for labels
# (Deletes folder if it exists for current day 
# - assuming it's easier for development purposes)
def create_folder():
  folderName = f'cambridge-{today.strftime("%d%m%y")}'
  
  # If directory exists, delete it
  if os.path.isdir(f'labels/{folderName}'):
    print(f'[i] {folderName} exists...removing it')
    shutil.rmtree(f'labels/{folderName}')
  
  # Create it
  os.mkdir(os.getcwd() + f'/labels/{folderName}')
  print(f'[i] {folderName} Directory Created')
  
  # Return folder name for use later
  return folderName

# Iterate data (from file) and create 
# label documents for 10 venues at a time
def create_documents(data, folderName):
  print('[i] Creating label documents')
  i=0
  for group in data:
    print(f'[{i} - {i+10}] Creating File')
    create_document(group, folderName, i)
    print(f'[{i} - {i+10}] Created')
    i = i + 10

# Ensure special characters are shown on labels
# Add extra chars here if and when necessary
def encodeSpecialChars(s):
  return s.replace("&", "&#38;")

# Create array with all of the info needed for 
# rendering the label - name & address
def extractContextData(group):
  contextData = {}
  i=0
  for g in group:
    try:
      address = getAddress(g['addresses'][0])
      contextData[i] = {
        'name': encodeSpecialChars(g['name']),
        'addr_line_1': address[0],
        'addr_line_2': address[1],
        'addr_line_3': address[2],
        'addr_line_4': address[3],
        'addr_line_5': address[4],
        'addr_line_6': address[5]
      } 
      i = i + 1
    except KeyError:
      print("[i] Group finished with less than 10 labels.")
      break
  return contextData

# Fill in 'empty' array elements - basically pad the array
# out with empty array elements so we can reference those indexes
# later on without any issues (same as addr padding foo).
def addContextArrayPadding(context):
  paddedContextArray = []
  for i in range(10):
    try:
      paddedContextArray.append(context[i])
    except KeyError:
      paddedContextArray.append({
        'name': '',
        'addr_line_1': '',
        'addr_line_2': '',
        'addr_line_3': '',
        'addr_line_4': '',
        'addr_line_5': '',
        'addr_line_6': ''
      })
  return paddedContextArray
  
# Create a single Word label document
def create_document(group, folderName, number):
  doc = DocxTemplate("templates/ten_labels.docx")

  contextData = extractContextData(group)
  contextData = addContextArrayPadding(contextData)
  context = createContextObject(contextData)

  doc.render(context)
  doc.save(f'labels/{folderName}/{number}-{number+10}.docx')

# Start
if __name__ == '__main__':
  with open('data.json', 'r', encoding='utf-8') as venues:
    print('[i] File Read')
    data = json.load(venues)
    folderName = create_folder()
    # Sort alphabetically by venue name
    sortedValues = sorted(data.values(), key=lambda item: item['name'])
    # Group into groups of 10
    groups = grouper(sortedValues, 10, {})
    create_documents(groups, folderName)
    print('Completed.')
