import csv
import json

# Read data.json file contents
def readDataJsonFile():
  with open('data.json', 'r') as f:
    return json.load(f)

# Write id & name to csv list
def appendToCSV(data):
  with open('sent_list.csv', "a") as csv_file:
    writer = csv.writer(csv_file, delimiter=',')
    for line in data.values():
      writer.writerow([line['id'] + '|' + line['name']])

# Start
if __name__ == '__main__':
  # Read data.json
  datajson = readDataJsonFile()
    
  # Append each id, name and date to the sent list CSV
  appendToCSV(datajson)
