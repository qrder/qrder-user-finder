import googlemaps
import time
import csv
import json
from includes.places_to_ignore import placesToIgnore

# Coords for Future Reference
langstone_home = (51.604700, -2.900900)
newport_city_centre = (51.5848588, -3.003409)
cardiff_city_centre = (51.5439273, -3.1369726)
cardiff_coryton = (51.5458248, -3.1812689)
cambridge_city_centre = (52.205376, 0.120148)
cambridge_north = (52.277873, 0.121072)
cambridge_east = (52.203785, 0.236023)
cambridge_south = (52.134381, 0.123760)
cambridge_west = (52.203890, 0.002498)
bristol_city_centre = (51.453802, -2.597298)
bury_st_edmunds_city_centre = (52.246037, 0.712517)

gmaps = googlemaps.Client(key='AIzaSyCGpZ-tsxmec3iVycbdGoC6UfNwhjjZWaA')
# radius is in meters (e.g. 10000 == 10km)
multipleCoords = [
  {"coords": cambridge_north, "radius": 8000},
  {"coords": cambridge_east, "radius": 8000},
  {"coords": cambridge_south, "radius": 8000},
  {"coords": cambridge_west, "radius": 8000},
  {"coords": bury_st_edmunds_city_centre, "radius": 1000}
]

# Get places nearby based on a keyword
# If next_page_token is present, we're getting 2nd/3rd page results
def makeSinglePageRequest(kw, next_page_token, coords, radius):
  params = { 'keyword': kw, 'location': coords, 'radius': radius }

  if next_page_token:
    params['page_token'] = next_page_token

  return gmaps.places_nearby(**params)

# Make API call for next page of results
def getNextPageResults(kw, prevResults, coords, radius):
  if 'next_page_token' in prevResults:
    time.sleep(2)
    return makeSinglePageRequest(kw, prevResults['next_page_token'], coords, radius)
  else:
    return {'results': []}

# Make API calls to get results
# Max we can get with 1 'call' is 60 results (or 3 pages),
# so to get more differing results, we need to change the original coords.
def getPlacesByKeyword(kw, coords, radius):
  pageOneResults = makeSinglePageRequest(kw, '', coords, radius)
  pageTwoResults = getNextPageResults(kw, pageOneResults, coords, radius)
  pageThreeResults = getNextPageResults(kw, pageTwoResults, coords, radius)

  return pageOneResults['results'] + pageTwoResults['results'] + pageThreeResults['results']

# Iterate array of places and print the names
def printPlaceNames(places):
  for place in places:
    print(place['name'])

# Write to file
def writeToFile(filename, dataToWrite):
  with open(filename, 'w', encoding='utf-8') as f:
    json.dump(dataToWrite, f, ensure_ascii=False, indent=4)

# Filter out data that is not needed & remove duplicates
def filterResultData(data):
  filteredData = {}
  for d in data:
    filteredData[d['place_id']] = {
      'id': d['place_id'],
      'name': d['name'],
      'status': d['business_status'] if 'business_status' in d else 'NOT FOUND',
      'rating': d['rating'] if 'rating' in d else 0,
      'location': {
        'lat': d['geometry']['location']['lat'],
        'lng': d['geometry']['location']['lng']
      },
      'addresses': []
    }
  return filteredData

# Check for duplicates already in data.json 
def checkAgainstDataJSON(data):
  with open('data.json') as dataJSON:
    datajson = json.load(dataJSON)
    origlength = len(datajson)
    for k,v in data.items():
      if k not in datajson.keys():
        datajson[k] = v
    difference = len(datajson) - origlength
    print(f"[i] Found {difference} new venues to add to data.json.")
    return datajson

# Remove lowest rated venues (0 or 1)
def refineData(data):
  refinedData = {}
  for k,v in data.items():
    if v['rating'] > 1:
      refinedData[k] = v
  return refinedData

# Check place name against list of franchise names
def placeIsNotFranchise(placeName):
  for f in placesToIgnore:
    if f in placeName:
      return False
  return True

# Remove franchises (in list at top of script)
def removeFranchises(data):
  return {k:v for k,v in data.items() if placeIsNotFranchise(v['name'])}

# Check place id against sent list csv
def placeHasNotBeenSentAlready(sentIds, placeId):
  for p in sentIds:
    if p in placeId:
      return False
  return True

# Remove previously sent to (in file)
def removePreviouslySent(data):
  sentIds = []
  with open('sent_list.csv', "r", newline='') as sent_list_csv:
    reader = csv.reader(sent_list_csv, delimiter=',')
    for row in reader:
      joined = ', '.join(row)
      split = joined.split('|') # [0] = id, [1] = name
      sentIds.append(split[0])
  # check data against list of ids 
  return {k:v for k,v in data.items() if placeHasNotBeenSentAlready(sentIds, v['id'])}

# Convert lat/lng to readable address
def getAddresses(data):
  dataWithAddress = data
  for value in dataWithAddress.values():
    addresses = gmaps.reverse_geocode((
      value['location']['lat'], 
      value['location']['lng']
    ))
    formattedAddresses = [d['formatted_address'] for d in addresses]
    i = 0
    while len(formattedAddresses) >= 3:
      formattedAddresses.pop()
      i += 1
    value['addresses'] = formattedAddresses
  return dataWithAddress

# Some lat/lng coords get converted into an 'unnamed road' address.
# These letters get returned to us so are essentially wasted.
# So we need to remove them.  It only removes them if the first address
# in the list is 'unnamed road' - if the second one is 'unnamed road', it
# is assumed that the first address is legitimate.
def removePlacesWithoutAddress(data):
  return {k:v for k,v in data.items() if "Unnamed Road" not in v['addresses'][0]}

# Sort func to pass to sort function.
# If 'rating' info does not exist, assume rating of 0
def sortFunc(d):
  return d['rating'] if 'rating' in d else 0

# Start
if __name__ == '__main__':
  if type(multipleCoords) is not list:
    multipleCoords = list(multipleCoords)
  
  # Get some parameters
  resetDataJson = input('Reset data.json? [y/n]: ')
  if not resetDataJson or resetDataJson == 'y':
    writeToFile('data.json', {})
    print("[i] Data.json reset.")
  appendToExistingData = input('Append to existing data? (If multiple coords are given, this will append them all) [y/n]: ')
  if not appendToExistingData:
    appendToExistingData = 'y'  

  i = 1
  for coords in multipleCoords:
    # Make API calls to get data
    print(f"[1] Making API Calls...Coords #{i}")
    restaurants = getPlacesByKeyword('restaurant', coords['coords'], coords['radius'])
    cafes = getPlacesByKeyword('cafe', coords['coords'], coords['radius'])
    pubs = getPlacesByKeyword('pub', coords['coords'], coords['radius'])
    bars = getPlacesByKeyword('bar', coords['coords'], coords['radius'])
    food = getPlacesByKeyword('food', coords['coords'], coords['radius'])
    hotels = getPlacesByKeyword('hotel', coords['coords'], coords['radius'])
    print("[1] Making API Calls...Done")

    # Combine data
    print("[2] Combining API Data...")
    combinedPlaces = restaurants + cafes + pubs + bars + food + hotels
    print("[2] Combining API Data...Done")

    # Sort based on rating (highest to lowest)
    print("[3] Sorting data...")
    combinedPlaces.sort(reverse=True, key=sortFunc)
    print("[3] Sorting data...Done")

    # Filter out the info we definitely need & remove duplicates
    print("[4] Filtering data...")
    filteredData = filterResultData(combinedPlaces)
    print("[4] Filtering data...Done")

    # If we want to append to data.json, check for duplicates in there too
    if appendToExistingData == 'y':
      print("[5] Check against current data.json...")
      filteredData = checkAgainstDataJSON(filteredData)
      print("[5] Check against current data.json...Done")
    else:
      print("[5] Skipping as not appending to existing data.json.")

    # Remove lowest ratings (0 & 1)
    print("[6] Removing lowest ratings...")
    refinedData = refineData(filteredData)
    print("[6] Removing lowest ratings...Done")

    # Remove franchises
    print("[7] Removing franchises...")
    removedFranchises = removeFranchises(refinedData)
    print("[7] Removing franchises...Done")

    # Remove venues that we have already sent to
    print("[8] Removing previously sent to...")
    removedPrevSentTo = removePreviouslySent(removedFranchises)
    print("[8] Removing previously sent to...Done")

    # Convert lat/lng to addresses
    print("[9] Converting lat/lng to addresses...")
    readableAddresses = getAddresses(removedPrevSentTo)
    print("[9] Converting lat/lng to addresses...Done")

    print("[10] Removing places with 'Unnamed road' address...")
    legitimateAddresses = removePlacesWithoutAddress(readableAddresses)
    print("[10] Removing places with 'unnamed road' address...Done")

    # Write final form of data to file
    print("[11] Writing users to file...")
    writeToFile('data.json', legitimateAddresses)
    print("[11] Writing users to file...Done")

    # Print some info about number of venues found
    print()
    print("Total Venues Found: " + str(len(combinedPlaces)))
    print("Total Removing Duplicates: " + str(len(filteredData)))
    print("Total Removing Low Ratings: " + str(len(refinedData)))
    print("Total Removing Franchises: " + str(len(removedFranchises)))
    print("Total Removing Previously Sent: " + str(len(removedPrevSentTo)))
    print("Total Removing 'Unnamed Road' Addresses: " + str(len(legitimateAddresses)))
    print("Total: " + str(len(legitimateAddresses)))

    i += 1