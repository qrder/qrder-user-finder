# These are substrings that people add to their name on Google.
# Things like "Cambridge", "an IHG Hotel", etc.
stringEndsToRemove = [
  " - Cambridge",
  " (Cambridge)",
  " Cambridge", 
  " Trumpington",
  ", Bury St Edmunds",
  " - Bury St Edmunds",
  " Bury St Edmunds",
  " Bury St.Edmunds",
  ", an IHG Hotel",
  " - Cardiff",
  " Cardiff",
  " - Newport",
  " Newport"
]
