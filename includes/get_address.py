import re

postcodePattern = r'[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][A-Z]{2}'

# Split an address into it's component parts
# First 2 lines are to get index of postcode and add comma before it
def splitAddressIntoArray(address):
  try:
    postcodeIndex = re.search(postcodePattern, address).start()
    address = address[:postcodeIndex-1] + ',' + address[postcodeIndex-1:]
    return address.split(',')
  except AttributeError:
    raise Exception("A postcode might be incorrect.  Address: " + address) from None

# Remove all items in address after postcode 
# element as they are not needed.
def removeExtraAddressElements(address):
  newAddress = []
  for el in address:
    newAddress.append(el.strip())
    if re.match(postcodePattern, el.strip()):
      break
  return newAddress

# Fill in 'empty' array elements - basically pad the array
# out with empty array elements so we can reference those indexes
# later on without any issues.
def addAddrArrayPadding(address):
  paddedArray = []
  for i in range(6):
    try:
      paddedArray.append(address[i])
    except IndexError:
      paddedArray.append('')
  return paddedArray

# Call the above 2 functions to create the address array.
# These split the string address param into an array,
# and then remove all array elements after the postcode.
def getAddress(address):
  addrArray = splitAddressIntoArray(address)
  removeElsAfterPostcode = removeExtraAddressElements(addrArray)
  return addAddrArrayPadding(removeElsAfterPostcode)